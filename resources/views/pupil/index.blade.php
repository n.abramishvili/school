@extends('layouts.pupil')

@section('title', 'მოსწავლე')

@section('content')
<link href="{{url('css/profile.css')}}" rel="stylesheet">
<div class="container">
      <div class="row">
      <div class="col-md-5  toppad  pull-right col-md-offset-3 ">
           <A href="#" >Edit Profile</A>

        <A href="{{url('logout')}}" >Logout</A>
       <br>
      </div>
        <div  class="col-md-8 col-md-offset-1" >
   
          <div class="panel panel-info">
            <div class="panel-heading">
              <h3 class="panel-title">{{$pupil->name.' '.$pupil->surname}}</h3>
            </div>
            <div class="panel-body">
              <div class="row">
                <div class="col-md-3 col-lg-3 " align="center"> <img alt="User Pic" src="{{asset('imgs/avatar-300x300.png')}}" class="img-circle img-responsive"> </div>
                
                <!--<div class="col-xs-10 col-sm-10 hidden-md hidden-lg"> <br>
                  <dl>
                    <dt>DEPARTMENT:</dt>
                    <dd>Administrator</dd>
                    <dt>HIRE DATE</dt>
                    <dd>11/12/2013</dd>
                    <dt>DATE OF BIRTH</dt>
                       <dd>11/12/2013</dd>
                    <dt>GENDER</dt>
                    <dd>Male</dd>
                  </dl>
                </div>-->
                <div class=" col-md-9 col-lg-9 "> 
                  <table class="table table-user-information">
                    <tbody>
                      <tr>
                        <td>Class</td>
                        <td>{{$pupil->SchoolClass->realName()}}</td>
                      </tr>
                      <tr>
                        <td>Date of Birth</td>
                        <td>---</td>
                      </tr>
                   
                         <tr>
                             <tr>
                        <td>Gender</td>
                        <td>---</td>
                      </tr>
                      <tr>
                        <td>Email</td>
                        <td><a href="#">{{$pupil->user->email}}</a></td>
                      </tr>
                        <td>Phone Number</td>
                        <td>---
                        </td> 
                      </tr>
                      </tr>
                        <td>Personal Number</td>
                        <td>{{$pupil->personal_number}}
                        </td> 
                      </tr>
                     
                    </tbody>
                  </table>
                </div>
              </div>
            </div>
            
          </div>
        </div>
      </div>
    </div>

@endsection