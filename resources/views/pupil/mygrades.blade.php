@extends('layouts.pupil')

@section('title', 'ნიშნები')

@section('content')

    <div class="col-md-4 pull-right">
    	{!!
    		Form::select(
    			'grade',
    			$semesters,
    			$selectedID,
    			['class' => 'form-control','id'=>"semester"]
    		)
    	!!}
    </div>
	@foreach($subjects as $key => $subject)
	<label>{{$subs[$key]}}</label>
	<br>

	<table  class="table table-striped">
			<thead>
	      <tr>
	        <th>თარიღი</th>
	        <th>ნიშანი</th>
	      </tr>
	    </thead>
		@foreach($subject as $grade)
		  <tr>
		    <td>{{$grade->date}}</td>
		    <td>{{$grade->grade}}</td>
		  </tr>
		@endforeach
	</table>
	<hr>
	@endforeach
@endsection