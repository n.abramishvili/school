@extends('layouts.admin')

@section('title', 'საგნები')

@section('content')
	<a class="btn btn-success pull-right" href="{{url('/admin/subjects/add')}}" >Add</a>
	<table  class="table table-striped table-bordered">
		<thead>
	      <tr>
	        <th>Name</th>
	        <th>Teachers</th>
	        <th>Classes</th>
	        <th>Edit</th>
	        <th>Delete</th>
	      </tr>
	    </thead>
		@foreach($subjects as $subject)
		  <tr>
		    <td>{{$subject->name}}</td>
		    <td>
		    	@foreach($subject->teachers as $teacher)
		    		{{$teacher->name.' '.$teacher->surname}}
		    	@endforeach	
		    </td>
		    <td>
		    		
		    </td>
		    <td><a href="{{url('/admin/subjects/edit/'.$subject->id)}}">რედაქტირება</a></td>
		    <td><a href="{{url('/admin/subjects/delete/'.$subject->id)}}" style="color:red">წაშლა</a></td>
		  </tr>
		@endforeach
	</table>
	

@endsection