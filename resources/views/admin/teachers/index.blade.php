@extends('layouts.admin')

@section('title', 'მართვა')

@section('content')
	<a class="btn btn-success pull-right" href="{{url('/admin/teachers/add')}}" >Add</a>
	<table  class="table table-striped table-bordered">
		<thead>
	      <tr>
	        <th>Firstname</th>
	        <th>Lastname</th>
	        <th>Personal number</th>
	        <th>Subjects</th>
	        <th>Edit</th>
	        <th>Delete</th>
	      </tr>
	    </thead>
		@foreach($teachers as $teacher)
		  <tr>
		    <td>{{$teacher->name}}</td>
		    <td>{{$teacher->surname}}</td>
		    <td>{{$teacher->personal_number}}</td>
		    <td>
		    	@foreach($teacher->subjects as $subject)
	      			{{$subject->name}}
	      		@endforeach	
		    </td>
		    <td><a href="{{url('/admin/teachers/edit/'.$teacher->id)}}">რედაქტირება</a></td>
		    <td><a href="{{url('/admin/teachers/delete/'.$teacher->id)}}" style="color:red">წაშლა</a></td>
		  </tr>
		@endforeach
	</table>
	{!! $teachers->render() !!}

@endsection
