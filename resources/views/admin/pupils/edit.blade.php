@extends('layouts.admin')

@section('title', 'დამატება')

@section('content')

	{!! Form::open([
		
		'url' => '/admin/pupils/'.$pupil->id,
		'method'=>'PUT',
		'class'=>'form-horizontal well'

		]) !!}
	<div>
			<fieldset>

			<!-- Form Name -->
			<legend>მოსწავლის რედაქტირება</legend>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label">სახელი</label>  
			  <div class="col-md-4">
			  <input  name="name" type="text" value="{{old('name',$pupil->name)}}" placeholder="სახელი" class="form-control input-md">  
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label">გვარი</label>  
			  <div class="col-md-4">
			  <input  name="surname" type="text" value="{{old('surname',$pupil->surname)}}" placeholder="გვარი" class="form-control input-md">  
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label">პირადი ნომერი</label>  
			  <div class="col-md-4">
			  <input  name="personal_number" type="text" value="{{old('personal_number',$pupil->personal_number)}}" placeholder="პირადი ნომერი" class="form-control input-md">  
			  </div>
			</div>	
			<!-- Select Basic -->
			<div class="form-group">
			  <label class="col-md-4 control-label" >კლასი</label>
			  <div class="col-md-4">
			 {!!
					Form::select(
						'class_id',
						$classes,
						old('class_id',$pupil->class_id),
						['class'=>'form-control']
						
						
					)
				!!}
			  </div>
			</div>
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<!-- Button -->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="singlebutton"></label>
			  <div class="col-md-4">
			    <button id="singlebutton" name="singlebutton" class="btn btn-primary">განახლება</button>
			  </div>
			</div>
			

			</fieldset>
		</div>
	{!! Form::close() !!}



@endsection