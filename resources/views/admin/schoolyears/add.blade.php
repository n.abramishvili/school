@extends('layouts.admin')

@section('title', 'დამატება')

@section('content')

	{!! Form::open(array('url' => '/admin/schoolyears')) !!}
	<div class="form-horizontal">
			<fieldset>

			<!-- Form Name -->
			<legend>სასწავლო წლის დამატება</legend>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label">წელი</label>  
				  <div class="col-md-2">
				  <input  name="year1" type="text" placeholder="დაწყების წელი" class="form-control input-md">
				  <input  name="year2" type="text" placeholder="დამთავრების წელი" class="form-control input-md"> 
				  </div>
			</div>

			<div class="form-group">
			  <label class="col-md-4 control-label" >სემესტრი</label>
			  <div class="col-md-2">
			    <select name="semester_type" class="form-control">
			        	<option value="1" >
			        		შემოდგომა
			        	</option>
			        	<option value="1" >
			        		გაზაფხული
			        	</option>
			    </select>
			  </div>
			</div>

			<!-- Button -->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="singlebutton"></label>
			  <div class="col-md-4">
			    <button id="singlebutton" name="singlebutton" class="btn btn-primary">დამატება</button>
			  </div>
			</div>
			

			</fieldset>
		</div>
	{!! Form::close() !!}



@endsection