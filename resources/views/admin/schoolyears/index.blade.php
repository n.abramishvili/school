@extends('layouts.admin')

@section('title', 'სასწავლო წელი')

@section('content')
	<a class="btn btn-success pull-right" href="{{url('/admin/schoolyears/add')}}" >Add</a>
	<table  class="table table-striped table-bordered">
		<thead>
	      <tr>
	        <th>SchoolYear</th>
	        <th>Semester</th>
	        <th>Status</th>
	        <th>Action</th>
	        <th>Delete</th>
	      </tr>
	    </thead>
		@foreach($schoolyears as $schoolyear)
		  <tr>
		    <td>{{$schoolyear->school_year}}</td>
		    <td>{{$schoolyear->semester()}}</td>
		    <td>
		    	@if($schoolyear->active==1)
		    		<div style="color:#40ff00" >{{$schoolyear->type()}}</div>
		    	@else
		    		{{$schoolyear->type()}}
		    	@endif		
		    </td>
		    <td>
		    	@if($schoolyear->active==0)
		    	<form role="form" method="POST" action="{{url('admin/schoolyears/active/'.$schoolyear->id)}}">
	                <button class="btn btn-success btn-sm" type="submit">გააქტიურება</button>
	                <input type="hidden" name="_token" value="{{ csrf_token() }}">  
            	</form>
            	@endif
		    </td>
		    <td>
		    	<a href="{{url('/admin/schoolyears/delete/'.$schoolyear->id)}}" style="color:red">წაშლა</a>
		    </td>
		  </tr>
		@endforeach
	</table>
	

@endsection