@extends('layouts.admin')

@section('title', 'რედაქტირება')

@section('content')

	{!! Form::open([
		
		'url' => '/admin/pupils/'.$pupil->id,
		'method'=>'PUT',

		]) !!}
	<div class="form-horizontal">
			<fieldset>

			<!-- Form Name -->
			<legend>სასწავლო წლის რედაქტირება</legend>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label">სახელი</label>  
			  <div class="col-md-2">
			  <input  name="name" type="text" value="{{old('name',$pupil->name)}}" placeholder="სახელი" class="form-control input-md">  
			  </div>
			</div>

			
			<!-- Select Basic -->
			<div class="form-group">
			  <label class="col-md-4 control-label" >სემესტრი</label>
			  <div class="col-md-1">
			 {!!
					Form::select(
						'semester_type',
						$semester,
						old('semester_type')
						['class'=>'form-control']
						
						
					)
				!!}
			  </div>
			</div>
			<input type="hidden" name="_token" value="{{ csrf_token() }}">
			<!-- Button -->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="singlebutton"></label>
			  <div class="col-md-4">
			    <button id="singlebutton" name="singlebutton" class="btn btn-primary">განახლება</button>
			  </div>
			</div>
			

			</fieldset>
		</div>
	{!! Form::close() !!}



@endsection