@extends('layouts.admin')

@section('title', 'რედაქტირება')

@section('content')
@if (count($errors) > 0)
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
			<p>{{ $error }}</p>
		@endforeach
	</div>
@endif

	{!! Form::open([
		
		'url' => '/admin/classes/'.$class->id,
		'method'=>'PUT',
		'class'=>'form-horizontal well'

		]) !!}
	<div class="form-horizontal">
			<fieldset>

			<!-- Form Name -->
			<legend>კლასის რედაქტირება</legend>

			<!-- Select Basic -->
			<div class="form-group">
			  <label class="col-md-4 control-label" >კლასი</label>
			  <div class="col-md-4">
			   {!!
					Form::select(
						'start_year',
						$classes,
						old('start_year',$year-$class->start_year),
						['class'=>'form-control']
						
						
					)
				!!}
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label">სახელი</label>  
			  <div class="col-md-4">
			  <input  name="name" type="text" value="{{old('name',$class->name)}}" placeholder="სახელი" class="form-control input-md">  
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label">კვოტა</label>  
			  <div class="col-md-4">
			  <input  name="max_pupil" type="text" value="{{old('max_pupil',$class->max_pupil)}}"  placeholder="კვოტა" class="form-control input-md">  
			  </div>
			</div>

			<!-- Button -->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="singlebutton"></label>
			  <div class="col-md-4">
			    <button id="singlebutton" name="singlebutton" class="btn btn-primary">შეცვლა</button>
			  </div>
			</div>
			

			</fieldset>
		</div>
	{!! Form::close() !!}



@endsection