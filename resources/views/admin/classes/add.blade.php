@extends('layouts.admin')

@section('title', 'დამატება')

@section('content')
@if (count($errors) > 0)
	<div class="alert alert-danger">
		@foreach($errors->all() as $error)
			<p>{{ $error }}</p>
		@endforeach
	</div>
@endif

	{!! Form::open(array('url' => '/admin/classes','class'=>'form-horizontal well')) !!}
	<div>
			<fieldset>

			<!-- Form Name -->
			<legend>კლასის დამატება</legend>

			<!-- Select Basic -->
			<div class="form-group">
			  <label class="col-md-4 control-label" >კლასი</label>
			  <div class="col-md-4">
			    <select name="start_year" class="form-control">
			    	@for($i=1;$i<=12;$i++)
			    		<option value="{{$i}}">{{$i}}</option>
			    	@endfor
			    </select>
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label">სახელი</label>  
			  <div class="col-md-4">
			  <input  name="name" type="text" placeholder="სახელი" class="form-control input-md">  
			  </div>
			</div>

			<!-- Text input-->
			<div class="form-group">
			  <label class="col-md-4 control-label">კვოტა</label>  
			  <div class="col-md-4">
			  <input  name="max_pupil" type="text" placeholder="კვოტა" class="form-control input-md">  
			  </div>
			</div>

			<!-- Button -->
			<div class="form-group">
			  <label class="col-md-4 control-label" for="singlebutton"></label>
			  <div class="col-md-4">
			    <button id="singlebutton" name="singlebutton" class="btn btn-primary">დამატება</button>
			  </div>
			</div>
			

			</fieldset>
		</div>
	{!! Form::close() !!}



@endsection