@extends('layouts.admin')

@section('title', 'კლასები')

@section('content')
	<a class="btn btn-success pull-right" href="{{url('/admin/classes/add')}}" >Add</a>
	<table  class="table table-striped table-bordered">
		<thead>
	      <tr>
	        <th>Class</th>
	        <th>Max Pupil</th>
	        <th>Teacher</th>
	        <th>Schedule</th>
	        <th>Edit</th>
	        <th>Delete</th>
	      </tr>
	    </thead>
		@foreach($classes as $class)
		  <tr>
		    <td>{{$year-$class->start_year.$class->name}}</td>
		    <td>{{$class->max_pupil}}</td>
		    <td>
		    	@if(isset($class->teacher->name))
		    		{{$class->teacher->name.' '.$class->teacher->surname}}
		    	@endif	
		    </td>
		    <td>
		    	<a href="{{url('/admin/classes/schedule/'.$class->id)}}">ცხრილი</a>
		     </td>
		    <td>
		    	<a href="{{url('/admin/classes/edit/'.$class->id)}}">რედაქტირება</a>
		    </td>
		    <td>
		    	<a href="{{url('/admin/classes/delete/'.$class->id)}}" style="color:red">წაშლა</a>
		    </td>
		  </tr>
		@endforeach
	</table>
	

@endsection