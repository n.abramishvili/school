@extends('layouts.teacher')

@section('title', 'მასწავლებელი')

@section('content')
	@foreach($subjects as $key => $subject)
	<label>{{$subs[$key]}}</label>
	<br>

	<table  class="table table-striped table-bordered">
			<thead>
	      <tr>
	        <th>თარიღი</th>
	        <th>ნიშანი</th>
	      </tr>
	    </thead>
		@foreach($subject as $grade)
		  <tr>
		    <td>{{$grade->date}}</td>
		    <td>{{$grade->grade}}</td>
		  </tr>
		@endforeach
	</table>
	{!! Form::open([

		'url' => 'teacher/pupils',
		'method'=>'POST',
		'class' => 'well  form-inline'

		]) !!}


		{!! Form::date('date', \Carbon\Carbon::now(),['class' => 'form-control col-md-2']) !!}

		<div class="col-md-2">
			{!!
				Form::select(
					'grade',
					$grade_options,
					old('grade'),
					['class' => 'form-control']
				)
			!!}
		</div>
		<input type="hidden" name="subject_id" value="{{$key}}" />
		<input type="hidden" name="pupil_id" value="{{$pupil_id}}" />

		<button type="submit" class="btn btn-success">ნიშნის დაწერა</button>

	{!! Form::close() !!}
	<hr>
	@endforeach
@endsection