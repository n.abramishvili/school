@extends('layouts.teacher')

@section('title', 'დავალებები')

@section('content')
	@foreach($subjects as $id=> $subject)
	<h4> {{$subject}}</h4>
		{!! Form::open([

			'url' => 'teacher/myclasses/'.$classID.'/homeworks',
			'method'=>'POST',
			'class' => 'well clearfix',

			]) !!}

			<input type="hidden" name="class_id" value="{{$classID}}" >
			<input type="hidden" name="subject_id" value="{{$id}}" >

			<textarea class="form-control" name="homework" placeholder="დავალება"></textarea>

			<br>
			<button type="submit" class="btn btn-success">დავალების დამატება</button>

		{!! Form::close() !!}
		@if(count($homeworks[$id])>0)
		<div class="alert alert-info">
			@foreach($homeworks[$id] as $homework)
				
				     {{$homework->date}}
				     <strong>{{$homework->homework}}</strong>
				     <br>
				
			@endforeach
		</div>
		@endif
	@endforeach
@endsection