@extends('layouts.teacher')

@section('title', 'მასწავლებელი')

@section('content')
	<table  class="table table-striped table-bordered">
		<thead>
	      <tr>
	        <th>კლასი</th>
	        <th>მოსწავლეები</th>
	        <th>დამრიგებელი</th>
	        <th>დავალებები</th>
	      </tr>
	    </thead>
		@foreach($classes as $class)
		  <tr>
		    <td>{{$year-$class->start_year.$class->name}}</td>
		    <td>
		    	<a href="{{url('/teacher/myclasses/'.$class->id.'/pupils')}}">ნახვა</a>
		    </td>
		    <td>
		    	@if(isset($class->teacher->name))
		    		{{$class->teacher->name.' '.$class->teacher->surname}}
		    	@endif	
		    </td>
		    <td><a href="{{url('/teacher/myclasses/'.$class->id.'/homeworks')}}">დავალებები</a></td>
		  </tr>
		@endforeach
	</table>
@endsection