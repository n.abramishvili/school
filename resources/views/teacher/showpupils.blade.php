@extends('layouts.teacher')

@section('title', 'მოსწავლეები')

@section('content')
	{!! Form::open([

			'url' => 'teacher/myclasses/'.$classID.'/pupils',
			'method'=>'POST',

			]) !!}

	<table  class="table ">
		<thead>
	      <tr>
	        <th>მოსწავლე</th>
	        <th>პირადი ნომერი</th>
	        <th>დასწრება</th>
	        <th>დასწრება {{$date}}
	        </th>
	      </tr>
	    </thead>
		@foreach($pupils as $pupil)
		  <tr>
		    <td>
		    	<a href="{{url('teacher/pupils/'.$pupil->id)}}">
		    	{{$pupil->name.' '.$pupil->surname}}</a>
		    </td>
		    <td>{{$pupil->personal_number}}</td>
		    <td>
		    	<a href="{{url('teacher/pupils/'.$pupil->id.'/attendance')}}">ნახვა</a>
		    </td>
		    <td>{{ Form::checkbox('attendance[]', $pupil->id)}}</td>
		  </tr>
		@endforeach
		<td></td>
		<td></td>
		<td></td>
		<td>
			<div class="col-md-6">
	        	{!!
		    		Form::select(
		    			'subject_id',
		    			$subjects,
		    			'',
		    			['class' => 'form-control']
		    		)
    			!!}
    			</div>
			<button type="submit" class="btn btn-primary">შევსება</button>
		</td> 
	</table>
	
	{!! Form::close() !!}
@endsection