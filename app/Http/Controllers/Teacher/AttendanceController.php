<?php

namespace App\Http\Controllers\Teacher;

use App\Http\Controllers\Controller;

use App\Models\Schedule;
use App\Models\SchoolClass;
use App\Models\SchoolYear;
use App\Models\Subject;
use App\Models\Pupil;
use App\Models\Teacher;
use App\Models\Grade;
use App\Models\Attendance;
use Illuminate\Http\Request;
use Exception;

class AttendanceController extends Controller
{

	public function index($pupilID)
	{
		$schoolyear=SchoolYear::active();
		$teacher=Teacher::authTeacher();
		$classID=Pupil::find($pupilID)->class_id;
		$subjects=Schedule::getTeacherSubjects($schoolyear->id,$classID,$teacher->id);
		$subs=[];
		foreach ($subjects as  $value) {
			$subs[$value->id]=$value->name;
		}

		$attendances=Attendance::orderBy('id', 'desc')->where([
			['school_year_id',$schoolyear->id],
 			['pupil_id',$pupilID],
 			['teacher_id',$teacher->id],
 		])->get();
 		$sub_att=[];
 		$att=[];
 		$sul=null;
 		foreach ($subs as $key => $value) {
 			$sub_att[$key]=$attendances->where('subject_id',$key);
 			$att[$key]['procenti']='---';
 			$att[$key]['daswreba']=$sub_att[$key]->where('attendance',"1")->count();
 			$att[$key]['gacdena']=$sub_att[$key]->where('attendance',"0")->count();
 			$sul=$att[$key]['daswreba']+$att[$key]['gacdena'];
 			if($sul>0){
 				$att[$key]['procenti']=
 				 round(($att[$key]['daswreba']/$sul)*100);
 			}

 		}
 		$viewData=[
	 		'subjects'=>$subs,
	 		'attendance'=>$sub_att,
	 		'stat'=>$att

 		];
 		return view('teacher/attendance',$viewData);
	}

 	public function store($classID,Request $request)
 	{	
 		$teacher=Teacher::authTeacher();
 		$schoolyear = SchoolYear::active();
 		if(!$teacher->subInClass($request->subject_id,$classID,$schoolyear->id)){
 			throw new Exception("Error Processing Request", 1);
 		}
 		if(is_array($request->attendance)){
	 		foreach ($request->attendance as  $att) {
	 			$pupil1=Pupil::find($att);
	 			if (!isset($pupil1)) {
	 				throw new Exception("Error Processing Request", 1);
	 			}
	 			elseif ($pupil1->class_id!=$classID) {
	 				throw new Exception("Error Processing Request", 1);
	 			}

	 		}
 		}
 		$date=\Carbon\Carbon::now();
 		$pupils=SchoolClass::find($classID)->pupils;
 		
 		foreach ($pupils as $pupil) {
 			$att=new Attendance;
 			$att->teacher_id=$teacher->id;
 			$att->school_year_id=$schoolyear->id;
 			$att->subject_id=$request->subject_id;
 			$att->pupil_id=$pupil->id;
 			$att->date=$date;
 			if (is_array($request->attendance)&&in_array($pupil->id, $request->attendance)) {
 				$att->attendance=1;
 			}
 			else{
 				$att->attendance=0;
 			}
 			$att->save();

 		}
 		return redirect('teacher/myclasses/'.$classID.'/pupils');

 		
 	}


}
