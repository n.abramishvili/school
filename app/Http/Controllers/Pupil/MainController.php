<?php

namespace App\Http\Controllers\Pupil;

use App\Http\Controllers\Controller;

use App\Models\Schedule;
use App\Models\SchoolClass;
use App\Models\SchoolYear;
use App\Models\Subject;
use App\Models\Pupil;
use App\Models\Teacher;
use App\Models\Grade;
use App\Models\Homework;
use App\Models\Attendance;
use Illuminate\Http\Request;

class MainController extends Controller
{
	public function index()
	{
		$pupil=Pupil::authPupil();
		return view('pupil/index',['pupil'=>$pupil]);
	}

	public function myGrades($id=null)
	{

		$pupil=Pupil::authPupil();
		$schoolyear = SchoolYear::active();
		if($id){
			$schoolyear = SchoolYear::find($id);
		}
		$grades=Grade::where([
 			['school_year_id',$schoolyear->id],
 			['pupil_id',$pupil->id],
 		])->get();
		$subjects=Subject::all();
 		$subs=[];
 		$schoolyears=SchoolYear::all();
 		$semesters=[];
 		foreach ($schoolyears as $value) {
 			$semesters[$value->id]=$value->school_year.' '.$value->semester();
 		}

 		foreach ($subjects as $value) {
 			$subs[$value->id]=$value->name;
 		}

 		$class_sub=Schedule::getClassSubjects($schoolyear->id,$pupil->class_id);
 		$gradeSubject=[];
 		foreach ($class_sub as  $subject) {
 			$gradeSubject[$subject->id]=[];
 		}
 		foreach ($grades as  $grade) {
 			$gradeSubject[$grade->subject_id][]=$grade;
 		}
 		$viewData=[
 			'subjects'=>$gradeSubject,
 			'subs'=>$subs,
 			'semesters'=>$semesters,
 			'selectedID'=>$schoolyear->id
 		];
 		return view('pupil/mygrades',$viewData);
		
	}

	public function schedule()
	{
		$weekday=[];
 		$pupil=Pupil::authPupil();
 		$schoolyear = SchoolYear::active();
 		$schedule=Schedule::where([
 			['school_year_id',$schoolyear->id],
 			['class_id',$pupil->class_id],
 		])->orderBy('order', 'asc')->get();
 		
 		for ($i = 1; $i <= count(config('school.days')); $i++) {
            $weekday[$i] = $schedule->where('week_day', (string) $i);
        }
 		return view('pupil/myschedule',['weekday'=>$weekday]);
	}

	public function homework()
	{
		$schoolyear=SchoolYear::active();
		$pupil=Pupil::authPupil();
		$subjects=Schedule::getClassSubjects($schoolyear->id,$pupil->class_id);
		$subs=[];
		foreach ($subjects as  $value) {
			$subs[$value->id]=$value->name;
		}

		$homeworks=Homework::orderBy('id', 'desc')->where([
			['school_year_id',$schoolyear->id],
 			['class_id',$pupil->class_id],
 		])->get();
 		$sub_homeworks=[];
 		foreach ($subs as $key => $value) {
 			$sub_homeworks[$key]=$homeworks->where('subject_id',$key);
 		}
 		$viewData=[
	 		'subjects'=>$subs,
	 		'homeworks'=>$sub_homeworks

 		];

		return view('pupil/myhomework',$viewData);
	}

	public function attendance()
	{
		$schoolyear=SchoolYear::active();
		$pupil=Pupil::authPupil();
		$subjects=Schedule::getClassSubjects($schoolyear->id,$pupil->class_id);
		$subs=[];
		foreach ($subjects as  $value) {
			$subs[$value->id]=$value->name;
		}

		$attendances=Attendance::orderBy('id', 'desc')->where([
			['school_year_id',$schoolyear->id],
 			['pupil_id',$pupil->id],
 		])->get();
 		$sub_att=[];
 		$att=[];
 		$sul=null;
 		foreach ($subs as $key => $value) {
 			$sub_att[$key]=$attendances->where('subject_id',$key);
 			$att[$key]['procenti']='---';
 			$att[$key]['daswreba']=$sub_att[$key]->where('attendance',"1")->count();
 			$att[$key]['gacdena']=$sub_att[$key]->where('attendance',"0")->count();
 			$sul=$att[$key]['daswreba']+$att[$key]['gacdena'];
 			if($sul>0){
 				$att[$key]['procenti']=
 				 round(($att[$key]['daswreba']/$sul)*100);
 			}

 		}
 		$viewData=[
	 		'subjects'=>$subs,
	 		'attendance'=>$sub_att,
	 		'stat'=>$att

 		];
 		return view('pupil/attendance',$viewData);
	}
}
