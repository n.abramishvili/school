<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Teacher;
use App\Models\SchoolYear;

class SchoolYearController extends Controller
{
    /**
     * Show the profile for the given user.
     *
     * @param  int  $id
     * @return Response
     */
    public function index()
    {
        $schoolyears=SchoolYear::all();
    	return view('admin/schoolyears/index',['schoolyears'=>$schoolyears]);
    }

   public function active($id)
   {
       $schoolyear=SchoolYear::find($id);
       $schoolyear->makeactive();
       $schoolyear->save();
       return redirect('admin/schoolyears');
   }

   public function add()
   { 
       return view('admin/schoolyears/add');
   }

   public function store(Request $request)
   {    
        $schoolyear=new SchoolYear;
        $year=[$request->year1,$request->year2];
        $schoolyear->school_year=implode('-', $year);
        $schoolyear->semester_type=$request->semester_type;
        $schoolyear->save();
       return redirect('admin/schoolyears');
   }




}