<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
 */

Route::get('/', function () {
    return view('welcome');
});
Route::group([
    'namespace' => 'Admin',
    'prefix' => 'admin',
    'middleware' => ['role:admin'],
], function () {

    Route::get('/', function () {
        return view('admin/index');
    });

    Route::get('teachers', 'TeacherController@index');
    Route::get('teachers/add', 'TeacherController@add');
    Route::post('teachers', 'TeacherController@store');
    Route::get('teachers/edit/{id}', 'TeacherController@edit');
    Route::put('teachers/{id}', 'TeacherController@update');

    Route::get('users', 'UserController@index');

    // Route::get('pupils/addtoclass', 'PupilController@showClass');
    // Route::post('pupils/addtoclass', 'PupilController@addToClass');
    // Route::get('classes', 'ClassController@showClasses');

    Route::get('pupils', 'PupilController@index');
    Route::get('pupils/add', 'PupilController@add');
    Route::post('pupils', 'PupilController@store');
    Route::get('pupils/edit/{id}', 'PupilController@edit');
    Route::put('pupils/{id}', 'PupilController@update');

    Route::get('subjects', 'SubjectController@index');
    Route::get('subjects/add', 'SubjectController@add');
    Route::post('subjects', 'SubjectController@store');
    Route::get('subjects/edit/{id}', 'SubjectController@edit');
    Route::put('subjects/{id}', 'SubjectController@update');

    Route::get('schoolyears', 'SchoolYearController@index');
    Route::post('schoolyears/active/{id}', 'SchoolYearController@active');
    Route::get('schoolyears/add', 'SchoolYearController@add');
    Route::post('schoolyears', 'SchoolYearController@store');

    Route::get('classes', 'ClassController@index');
    Route::get('classes/add', 'ClassController@add');
    Route::post('classes', 'ClassController@store');
    Route::get('classes/edit/{id}', 'ClassController@edit');
    Route::put('classes/{id}', 'ClassController@update');
    Route::get('classes/schedule/{id}', 'ClassController@schedule');
    Route::post('classes/schedule/save/{id}', 'ClassController@saveSchedule');
    Route::get('classes/schedule/teacher/options/{subjectId}', 'ClassController@selectSubject');

    Route::get('classes/ajax-test/', 'ClassController@ajaxTest');

});

// Authentication Routes...
Route::get('login', 'Auth\AuthController@showLoginForm');
Route::post('login', 'Auth\AuthController@login');
Route::get('logout', 'Auth\AuthController@logout');


// Password Reset Routes...
Route::get('password/reset/{token?}', 'Auth\PasswordController@showResetForm');
Route::post('password/email', 'Auth\PasswordController@sendResetLinkEmail');
Route::post('password/reset', 'Auth\PasswordController@reset');

Route::get('/home', 'HomeController@index');

Route::group([
    'namespace' => 'Teacher',
    'prefix' => 'teacher',
    'middleware' => ['role:teacher'],
], function () {

    Route::get('/', 'MainController@index');

    Route::get('myclasses', 'MainController@classes');
    Route::get('myschedule', 'MainController@schedule');
    Route::get('myclasses/{id}/pupils', 'MainController@showPupils');
    Route::post('myclasses/{id}/pupils', 'AttendanceController@store');
    Route::get('pupils/{id}', 'GradeController@index');
    Route::get('pupils/{id}/attendance', 'AttendanceController@index');
    Route::post('pupils/', 'GradeController@store');
    Route::get('myclasses/{id}/homeworks', 'HomeworkController@index');
    Route::post('myclasses/{id}/homeworks', 'HomeworkController@store');
});

Route::group([
    'namespace' => 'Teacher',
    'prefix' => 'teacher',
    'middleware' => ['role:tutor'],
], function () {

    Route::get('myclass', 'TutorController@index');
    Route::get('myclass/pupils/{id}', 'TutorController@pupilGrade');
    Route::get('myclass/homeworks', 'TutorController@homework');
    Route::get('myclass/pupils/{id}/attendance', 'TutorController@attendance');


});

Route::group([
    'namespace' => 'Pupil',
    'prefix' => 'pupil',
    'middleware' => ['role:pupil'],
], function () {

    Route::get('/', 'MainController@index');
    Route::get('mygrades/{id?}', 'MainController@myGrades');
    Route::get('myschedule', 'MainController@schedule');
    Route::get('myhomeworks', 'MainController@homework');
    Route::get('attendance', 'MainController@attendance');


});