<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;
use App\Models\Schedule;
use App\Models\Pupil;

class Teacher extends Model
{
    public function subjects()
    {
        return $this->belongsToMany('App\Models\Subject');
    }

    public function schoolclass()
    {
        return $this->belongsTo('App\Models\SchoolClass','class_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User');
    }

    public function makeClassTeacher($class_id)
    {
    	if($class_id==0){
    		$class_id=NULL;
    	}
       $teacher=Teacher::where('class_id',$class_id)->first();
       if(isset($teacher->class_id)){
	       $teacher->class_id=NULL;
	       $teacher->save();

       }      
       $this->class_id=$class_id; 
    }

    public static function hasSubject($subjectID)
    {
      $teachersArr=[];
      $teachers=Teacher::all();

      foreach ($teachers as $teacher) {
        foreach ($teacher->subjects as $subject) {
          if($subject->id==$subjectID){
            $teachersArr[]=$teacher;
            break;
          }
        }
      }

      return $teachersArr;

    }

    public static function authTeacher($value='')
    {
        $user = Auth::user();
        $teacher=Teacher::where('user_id',$user->id)->first();
        if(isset($teacher)){
          return $teacher;
        }
        return 0;
        
    }

    public function hasClass($classId,$schoolyearId)
    {
      $schedule=Schedule::where([
        ['teacher_id',$this->id],
        ['school_year_id',$schoolyearId],
        ])->get();
      foreach ($schedule as $value) {
        if($value->SchoolClass->id==$classId)
          return true;
      }
      return false;
    }

    public function hasPupil($PupilId,$schoolyearId)
    {
      $schedule=Schedule::where([
        ['teacher_id',$this->id],
        ['school_year_id',$schoolyearId],
        ])->get();
      foreach ($schedule as $value) {
        foreach ($value->SchoolClass->pupils as  $pupil) {
          if($pupil->id==$PupilId){
            return true;
          }
        }
      }
      return false;
    }

    public function isPupilTutor($PupilId)
    {
      if(Pupil::find($PupilId)->class_id==$this->class_id){
        return true;
      }
        return false;
    }

    public function subInClass($subjectId,$classId,$schoolyearId)
    {
      $schedule=Schedule::where([
        ['teacher_id',$this->id],
        ['school_year_id',$schoolyearId],
        ['class_id',$classId],
        ['subject_id',$subjectId],
        ])->get();
      return count($schedule)>0;
    }
}
