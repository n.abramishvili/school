<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
     public function teachers()
    {
        return $this->belongsToMany('App\Models\Teacher');
    }

        public function classes()
    {
        return $this->belongsToMany('App\Models\SchoolClass','class_subject','subject_id','class_start_year');
    }
}
