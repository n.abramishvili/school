<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Auth;

class Pupil extends Model
{
    public function SchoolClass()
    {
        return $this->belongsTo('App\Models\SchoolClass','class_id');
    }

     public static function authPupil()
    {
        $user = Auth::user();
        $pupil=Pupil::where('user_id',$user->id)->first();
        if(isset($pupil)){
          return $pupil;
        }
        return 0;
        
    }
    public function user()
    {
        return $this->belongsTo('App\User');
    }
}
