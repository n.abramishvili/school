
$(document).ready(function(){

	$('#ScheduleForm').on('click','button',function(){
		$(this).parents('.wrapper').remove();
	})
	
	$('#addButton').click(function(){
		original_wrapper = $('#schedulePicker').find('.wrapper');
		var picker =  original_wrapper.clone();

		picker.find('select.subject').val(original_wrapper.find('select.subject').val());
		picker.find('select.day').val(original_wrapper.find('select.day').val());
		picker.find('select.teacher').val(original_wrapper.find('select.teacher').val());
		
		pickerButton = 	picker.find('button');
		
		pickerButton.find('i')
		.removeClass('glyphicon-plus')
		.addClass('glyphicon-remove');

		// pickerButton.click(function(){
		// 	picker.remove();
		// })
		
		//glyphicon glyphicon-remove
		picker.appendTo($('#ScheduleForm'));
		//picker.find('button')
	});



	$( "#scheduleWrapper" ).on('change','.subject',function() {
		var subID= $(this);
		$.ajax({
		  method: "GET",
		  url: "/admin/classes/schedule/teacher/options/"+subID.val(),
		  dataType: "json",
		  beforeSend : function(){
			$('#mainLoader').removeClass('hide');
		  },
		  complete : function(){
		  	$('#mainLoader').addClass('hide');
		  }
		})
		.done(function( data ) {
			var $el = subID.parents('.wrapper').find('.teacher');
			$el.empty();
			$.each(data, function(key,teacher) {
				$el.append($("<option></option>")
			      .attr("value", teacher.id).text(teacher.name+' '+teacher.surname));
			});
		}).error(function(){
			bootbox.alert("Incorrect json format!");
		});
	});


});