<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateClassSubjectTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
         Schema::create('class_subject', function (Blueprint $table) {
            $table->integer('class_start_year');
            $table->integer('subject_id')->unsigned();
            //$table->foreign('class_start_year')->references('start_year')->on('classes');
            $table->foreign('subject_id')->references('id')->on('subjects');
            $table->primary(['class_start_year', 'subject_id']);
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('class_subject');
    }
}
