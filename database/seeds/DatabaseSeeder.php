<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        factory(App\Models\Pupil::class,200)->create();
        $this->call(ClassesTableSeeder::class);
        factory(App\Models\Teacher::class,20)->create();
        $this->call(SchoolYearsTableSeeder::class);
        $this->call(SubjectTableSeeder::class);
        $this->call(SchedulesTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(RoleUserTableSeeder::class);
    }
}
