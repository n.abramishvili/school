<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('roles')->delete();
        $insertArray = [
        	[
            'name' => 'admin',
            ],
            [
            'name' => 'teacher',
            ],
            [
            'name' => 'tutor',
            ],
            [
            'name' => 'pupil',
            ],
             
        ];
        DB::table('roles')->insert($insertArray);
    }
}
