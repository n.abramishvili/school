<?php

use Illuminate\Database\Seeder;

class SubjectTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
       DB::table('subjects')->delete();
        $insertArray = [
        	[
             'name'=>'მათემატიკა'
            ],
            [
             'name'=>'ფიზიკა'
            ], 
            [
             'name'=>'ქიმია'
            ],
            [
             'name'=>'ისტორია'
            ],
            [
             'name'=>'გეოგრაფია'
            ],
        ];
        DB::table('subjects')->insert($insertArray);
    }
}
